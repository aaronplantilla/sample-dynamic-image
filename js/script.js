// Female Fade In

function femaleAnimate(){
    var elem = document.getElementById("spot-female");
    var id = setInterval(frame, 5);
    function frame(){
            elem.style.opacity = 1;
            clearInterval(id)
    }
   
}

// Headline1 Slide in
 function head1Animate(){
    var elem1 = document.getElementById("spot-head1");
    var id1 = setInterval(frame1, 1000);
    function frame1(){
            elem1.style.left = 0;
    }
}

// Female Fade Out
function femaleAnimateOut(){
    var elem2 = document.getElementById("spot-female");
    var id2 = setInterval(frame2, 3000);
    function frame2(){
            elem2.style.opacity = 0; 
    }
   
}

// Headline Out
function head1AnimateOut(){
    var elem3 = document.getElementById("spot-head1");
    var id3 = setInterval(frame3, 3000);
    function frame3(){
            elem3.style.opacity = 0;
            clearInterval(id3);
    }
   
}

// Headline2 Slide In
function head2Animate(){
    var elem4 = document.getElementById("spot-head2");
    var id4 = setInterval(frame4, 4000);
    function frame4(){
            elem4.style.left = 0;
    }
}

// Subhead Fade In
function subHeadAnimate(){
    var elem5 = document.getElementById("spot-subhead");
    var id5 = setInterval(frame5, 4500);
    function frame5(){
            elem5.style.opacity = 1;
            clearInterval(id5)
    }
   
}

// Button Fade In
function buttonAnimate(){
    var elem6 = document.getElementById("spot-button");
    var id6 = setInterval(frame6, 5000);
    function frame6(){
            elem6.style.opacity = 1;
            clearInterval(id6)
    }
   
}

// Logo Slide in
 function logoAnimate(){
    var elem7 = document.getElementById("spot-logo");
    var id7 = setInterval(frame7, 5500);
    function frame7(){
            elem7.style.left = 0;
    }
}

function resetAnimation(){

    var elem = document.getElementById("spot-female");
    var id = setInterval(frame, 5);
    function frame(){
            elem.style.opacity = 1;
            clearInterval(id)
    }

    var elem1 = document.getElementById("spot-head1");
    var id1 = setInterval(frame1, 1000);
    function frame1(){
            elem1.style.left = 0;
    }

    var elem2 = document.getElementById("spot-female");
    var id2 = setInterval(frame2, 3000);
    function frame2(){
            elem2.style.opacity = 0; 
    }

    var elem3 = document.getElementById("spot-head1");
    var id3 = setInterval(frame3, 3000);
    function frame3(){
            elem3.style.opacity = 0;
            clearInterval(id3);
    }

    var elem4 = document.getElementById("spot-head2");
    var id4 = setInterval(frame4, 4000);
    function frame4(){
            elem4.style.left = 0;
    }

    var elem5 = document.getElementById("spot-subhead");
    var id5 = setInterval(frame5, 4500);
    function frame5(){
            elem5.style.opacity = 1;
            clearInterval(id5)
    }

    var elem6 = document.getElementById("spot-button");
    var id6 = setInterval(frame6, 5000);
    function frame6(){
            elem6.style.opacity = 1;
            clearInterval(id6)
    }

    var elem7 = document.getElementById("spot-logo");
    var id7 = setInterval(frame7, 5500);
    function frame7(){
            elem7.style.left = 0;
    }
}